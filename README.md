# rofi-edit

![edit](/uploads/3233df592c9531a1f6c9673ee0971338/edit.png)

This is a simple rofi script to open dot files for editing.  It was inspired by rofi-beats (https://github.com/Carbon-Bl4ck/Rofi-Beats) and I have adapted the format used.

Edit the script and add your own links, along with your preferred terminal and editor.  I would also recommend that you use a key binding to launch it.

It isn't perfect at the moment.  Not all terminals work with it, but I've found urxvt, xterm and alacritty do work.  Sometimes the editor you open will only fill half the terminal space when used on tiling window managers.  I'm searching for a solution and will update the script when I find one.

UPDATE: I've added a sleep command into the file and it now works more consistently with alacritty and st.


## Who Am I?

My real name is Steve Anelay, but most people just call me OTB. I am the creator of the OldTechBloke Youtube channel ([https://www.youtube.com/c/OldTechBloke](https://www.youtube.com/c/OldTechBloke)). I&#39;m a Linux desktop enthusiast who has been installing and running Linux since 2004. I am definitely not a developer like many on GitLab, just a desktop user with a passion for all things Linux. My videos cover a range of topics, from distro reviews through to commentaries and Linux news items and debates.

## Licence

The content of this repository is licensed under the MIT Licence. Basically, that gives you the right to use, copy, modify, etc. the content how you see fit. You can read the full licence terms here ([https://opensource.org/licenses/MIT](https://opensource.org/licenses/MIT)).


